const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const api = require('./api');
const port = process.env.PORT || 4000;
const path = require('path');

// Body-parser
app.use(cors({
    origin: ['http://localhost:4001'],
    methods: ['GET', 'POST', 'PUT', 'DELETE'],
    credentials: true
}));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use("/api", api);
const server = app.listen(port);