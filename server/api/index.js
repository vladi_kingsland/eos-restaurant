const express = require("express");
const api = express.Router();

const fetch = require("./model/fetch");
const endpoints = require("./endpoints");

api.use("/eos", endpoints);
api.use("/fetch", fetch);

module.exports = api;
