const endpoints = require('express').Router();
const endpointsCtrl = require("./endpoints.ctrl");

//When Data Getting
endpoints.get("/chef-list", endpointsCtrl.getChefList);
endpoints.get("/menu-list", endpointsCtrl.getMenuList);
endpoints.get("/order-list", endpointsCtrl.getOrdersList);

//When Data Updating
endpoints.put("/regchef", endpointsCtrl.regchef);
endpoints.put("/createitem", endpointsCtrl.createitem);
endpoints.put("/updateitem", endpointsCtrl.updateitem);
endpoints.put("/deleteitem", endpointsCtrl.deleteitem);

module.exports = endpoints;
