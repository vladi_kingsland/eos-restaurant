const smartContractActions = require('../model/fetch/eosSmartContractActions');
const getTable = require('../model/fetch/getTable');

exports.getChefList = async (req, res) => {

    let result = "fail";
    let reason;

    const contractChefList = await getTable.getAllChefList();

    if(contractChefList === undefined){
        reason = "fetchFail";
        res.json({ result, reason });
        return;
    }

    result = "success";

    res.json({ result, contractChefList });

};

exports.getMenuList = async (req, res) => {
    // Fill this out
};

exports.getOrdersList = async (req, res) => {
    // Fill this out
};

exports.regchef = async (req, res) => {

    let result = "fail";
    let reason;

    const { account, chef_name } = req.body;
    const data = { account, chef_name };

    const contractRegchef = await smartContractActions.contractRegchef(data);

    if(contractRegchef === undefined){
        reason = "contractFail";
        res.json({ result, reason });
        return;
    }

    result = "success";

    res.json({ result });

};

exports.createitem = async (req, res) => {

    // Fill this out

};

exports.updateitem = async (req, res) => {

    // Fill this out

};

exports.deleteitem = async (req, res) => {

    // Fill this out

};