const httpEndpoint = 'https://jungle2.cryptolions.io:443';

const chainId = 'e70aaab8997e1dfce58fbfac80cbbb8fecec7b99cf982a9444273cbc64c41473';

const account = ''; // Fill this out

const chefTableData = {
    code: account,
    scope: account,
    table: '', // Fill this out
    json: true,
    index_position: 'primary',
    key_type: 'uint64_t',
    limit: -1,
};

const menuTableData = {
    code: account,
    scope: account,
    table: '', // Fill this out
    json: true,
    index_position: 'primary',
    key_type: 'uint64_t',
    limit: -1,
};

const orderTableData = {
    code: account,
    scope: account,
    table: '', // Fill this out
    json: true,
    index_position: 'primary',
    key_type: 'uint64_t',
    limit: -1,
};

const regchefConfig = {
    chainId,
    keyProvider: [''], // Fill this out
    httpEndpoint,
    broadcast: true,
    verbose: true,
    sign: true,
    authorization: 'account@regchef',
};

const createitemConfig = {
    chainId,
    keyProvider: [''], // Fill this out
    httpEndpoint,
    broadcast: true,
    verbose: true,
    sign: true,
    authorization: 'account@createitem',
};

const updateitemConfig = {
    chainId,
    keyProvider: [''], // Fill this out
    httpEndpoint,
    broadcast: true,
    verbose: true,
    sign: true,
    authorization: 'account@updateitem',
};

const deleteitemConfig = {
    chainId,
    keyProvider: [''], // Fill this out
    httpEndpoint,
    broadcast: true,
    verbose: true,
    sign: true,
    authorization: 'account@deleteitem',
};

module.exports = {

    chefTableData,

    menuTableData,

    orderTableData,

    regchefConfig,

    createitemConfig,

    updateitemConfig,

    deleteitemConfig,

    getHostEndPoint: "https://jungle2.cryptolions.io/v1/chain/get_table_rows",

    netUrl: 'jungle2.cryptolions.io',
    netProtocol: 'https',
    netPort: '443',

    chainId,
    httpEndpoint,
    account,
};