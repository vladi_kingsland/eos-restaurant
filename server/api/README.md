# API

### Description

`./constant/index.js` contains configurable variables that define the chain, RPC API endpoint, and smart contract account names.

`./model/fetch/getTable.js` contains the implementation of the functions that creates POST requests to pull on-chain data from the smart contract tables.

`./model/fetch/eosSmartContractActions.js` contains the implementation of the functions that push and sign transactions onto the chain.


