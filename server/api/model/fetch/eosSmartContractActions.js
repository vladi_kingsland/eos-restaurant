const constant = require('../../constant');
const Eos = require('eosjs');

const regchef = Eos(constant.regchefConfig);
const createitem = Eos(constant.createitemConfig);
const updateitem = Eos(constant.updateitemConfig);
const deleteitem = Eos(constant.deleteitemConfig);

module.exports = {

    async contractRegchef(data) {

        const account = data.account;
        const chef_name = data.chef_name;

        try {
            const result = await regchef.transaction(
                {
                    actions: [
                        {
                            account: 'constant.account',
                            name: 'regchef',
                            authorization: [{
                                actor: 'constant.account',
                                permission: 'regchef'
                            }],
                            data: {
                                account,
                                chef_name
                            }
                        }
                    ]
                }
            );
            console.log(result);
            return result;
        }
        catch (ex) {
            console.log(ex);
        }
    },

    async contractCreateitem(data) {
        // Fill this out
    },

    async contractUpdateitem(data) {
        // Fill this out
    },

    async contractDeleteitem(data) {
        // Fill this out
};