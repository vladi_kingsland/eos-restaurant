const constant = require('../../constant');
const request = require('request');
const url = constant.getHostEndPoint;

module.exports = {

    getAllChefList: function () {
        return new Promise((resolve, reject) => {
            request({
                url: url,
                method: "POST",
                json: true,
                body: constant.chefTableData,
            }, function (error, response, body) {
                if (error) throw error;
                resolve(body.rows);
            });
        });
    },

    getAllMenuList: function () {
        // Your code goes here
    },

    getAllOrderList: function () {
        // Your code goes here
    },
};