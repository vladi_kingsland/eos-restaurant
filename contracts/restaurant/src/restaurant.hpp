#pragma once

#include <eosiolib/eosio.hpp>
#include <eosiolib/time.hpp>
#include <eosiolib/asset.hpp>
#include <eosiolib/singleton.hpp>

#include <string>
#include <vector>
#include <algorithm>

namespace restaurant{
    using eosio::const_mem_fun;
    using eosio::datastream;
    using eosio::indexed_by;
    using eosio::name;
    using eosio::asset;
    using eosio::time_point_sec;
    using std::function;
    using std::string;
    using std::vector;

    class [[eosio::contract("restaurant")]] restaurant_contract : public eosio::contract{

    public:

        using contract::contract;

        restaurant_contract( name s, name code, datastream<const char*> ds )
                :contract(s,code,ds){}

        [[eosio::action]]
        void regchef(name account, const string& chef_name);

        [[eosio::action]]
        void createitem(/* Add appropriate parameters here */);

        [[eosio::action]]
        void updateitem(/* Add appropriate parameters here */);

        [[eosio::action]]
        void deleteitem(/* Add appropriate parameters here */);

        [[eosio::action]]
        void transfer(name sender, name receiver);

        [[eosio::action]]
        void claimorder(name chef, uint64_t item_key);

        [[eosio::action]]
        void rateitem(name customer, uint64_t item_key, uint64_t rating);

        [[eosio::action]]
        void updateconfig(uint64_t min_rating, uint64_t max_rating, uint64_t const3, uint64_t const4, uint64_t const5);

        struct ORDER_STATUS {
            const static uint8_t IN_PROGRESS = 1;
            const static uint8_t COMPLETED = 2;
        };

        struct [[eosio::table("chefs"), eosio::contract("restaurant")]] chef{
            name account;
            string chef_name;
            uint64_t orders_completed;

            uint64_t primary_key() const {return account.value; }

            EOSLIB_SERIALIZE(chef, (account)(chef_name)(orders_completed))
        };

        typedef eosio::multi_index<name("chefs"), chef> chefs;

        struct [[eosio::table]] config {
            uint64_t min_rating = 1;
            uint64_t max_rating = 5;
            uint64_t share;
            // Other config fields if ever needed
            uint64_t const4;
            uint64_t const5;

            uint64_t primary_key() const { return 0; }

            EOSLIB_SERIALIZE( config, (min_rating)(max_rating)(share)(const4)(const5) )
        };

        typedef eosio::singleton< "config"_n, config > config_singleton;

        struct [[eosio::table("menu"), eosio::contract("restaurant")]] item{
            uint64_t key;
            /*
             *  Add fields here
             */

            uint64_t primary_key() const { return key; }

            EOSLIB_SERIALIZE(item, /* add fields here */)
        };

        typedef eosio::multi_index<name("menu"), item> items;

        struct [[eosio::table("orders"), eosio::contract("restaurant")]] order{
            uint64_t order_key;
            uint64_t order_time;
            name customer;
            uint64_t item_key;
            uint8_t status = ORDER_STATUS::IN_PROGRESS;

            uint64_t primary_key() const { return order_key; }

            EOSLIB_SERIALIZE(order, (order_key)(order_time)(customer)(item_key)(status))
        };

        typedef eosio::multi_index<name("orders"), order> orders;

    private:

        struct account {
            asset    balance;

            uint64_t primary_key() const { return balance.symbol.code().raw(); }
        };

        typedef eosio::multi_index<name("accounts"), account> accounts;

        // taken from eosio.token.hpp
        struct st_transfer {
            name  from;
            name  to;
            asset         quantity;
            std::string   memo;
        };
    };
}//restaurant