#include "restaurant.hpp"

namespace restaurant{
    void restaurant_contract::regchef(name account, const string& chef_name){

    }

    void restaurant_contract::createitem(/* Add appropriate parameters here */){

    }

    void restaurant_contract::updateitem(/* Add appropriate parameters here */){

    }

    void restaurant_contract::deleteitem(/* Add appropriate parameters here */){

    }

    void restaurant_contract::transfer(name sender, name receiver){
        auto transfer_data = eosio::unpack_action_data<st_transfer>();

        if(transfer_data.from == get_self()){
            return;
        }

        eosio_assert(transfer_data.from == get_self() || transfer_data.to == get_self(), "Invalid transfer");

        eosio_assert(transfer_data.quantity.is_valid(), "Invalid asset");
    }

    void restaurant_contract::claimorder(name chef, uint64_t order_key){

    }

    void restaurant_contract::rateitem(name customer, uint64_t order_key, uint64_t rating){

    }

    void restaurant_contract::updateconfig(uint64_t min_rating, uint64_t max_rating, uint64_t share, uint64_t const4, uint64_t const5){

    }
}

#define EOSIO_DISPATCH_CUSTOM( TYPE, MEMBERS ) \
extern "C" { \
   void apply( uint64_t receiver, uint64_t code, uint64_t action ) { \
      auto self = receiver; \
      if( code == self || code == "eosio.token"_n.value ) { \
         if( action == "transfer"_n.value){ \
           eosio_assert( code == "eosio.token"_n.value, "Must transfer EOS"); \
         } \
         switch( action ) { \
            EOSIO_DISPATCH_HELPER( TYPE, MEMBERS ) \
         } \
         /* does not allow destructor of thiscontract to run: eosio_exit(0); */ \
      } \
   } \
}

EOSIO_DISPATCH_CUSTOM(restaurant::restaurant_contract, (regchef)(createitem)(updateitem)(deleteitem)(transfer)(claimorder)(rateitem)(updateconfig))